import {
  MainPage,
  ElementPage,
  ElementList,
  HomePage,
  PokeDexPage,
} from "./pages";
import { Navbar, PokemonViewer } from "./components";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  useParams,
} from "react-router-dom";
import "bootstrap/dist/css/bootstrap.min.css";

const App = () => {
  return (
    <Router>
      <div className="container">
        <Switch>
          <Route exact path="/">
            <HomePage />
          </Route>
          <div>
            <Navbar />
            <div>
              <Route exact path="/SearchPage">
                <MainPage />
              </Route>
              <Route exact path="/ElementPage">
                <div className="d-flex justify-content-center">
                  <ElementPage />
                </div>
              </Route>
              <Route exact path="/PokeDexPage">
                <div>
                  <PokeDexPage />
                </div>
              </Route>
              <Route path="/PokeDexPage/:Pokemon">
                <PokemonViewer />
              </Route>
              <Route path="/ElementPage/:Elements">
                <ElementList />
              </Route>
            </div>
          </div>
        </Switch>
      </div>
    </Router>
  );
};

export default App;

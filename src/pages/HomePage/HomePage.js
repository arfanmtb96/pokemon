import Pokeball from "../../assets/Pokeball.png";
import Pokecard from "../../assets/PokeCard.png";
import { useState } from "react";
import { Link } from "react-router-dom";
import "./Homepage.css";

const HomePage = () => {
  const [poke, setPoke] = useState([
    {
      image: Pokecard,
      name: "View PokeDex",
      elements: "/PokeDexPage",
      id: 1,
    },
    {
      image: Pokeball,
      name: "Search Pokemon Card",
      elements: "/SearchPage",
      id: 2,
    },
  ]);
  return (
    <div className="bg-dark">
      <h1 className="bg-danger text-white p-3 text-justify">
        My 1st Pokemon website
      </h1>
      <div className="d-flex flex-row flex-wrap justify-content-center">
        {poke.map((pokemon) => (
          <div key={pokemon.id} className="element-container m-4">
            <Link to={pokemon.elements}>
              <img src={pokemon.image} alt={pokemon.name} className="image" />
              <div className="middle">
                <div className="text">{pokemon.name}</div>
              </div>
            </Link>
          </div>
        ))}
      </div>
    </div>
  );
};

export default HomePage;

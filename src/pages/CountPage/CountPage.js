import { useEffect, useState, useMemo } from "react";

const CountPage = () => {
  const [count, setCount] = useState(0);
  const [dark, setDark] = useState(false);

  // console.log(count);

  const slowFunction = (num1) => {
    return new Promise((resolve, reject) => {
      for (let i = 0; i <= 1000000000; i++) {}
      let total = 0;
      total = num1 + 3;
      resolve(total);
    });
  };

  const onClickHandler = () => {
    slowFunction(count + 1).then((data) => console.log(data));
    return;
  };

  // const addNumber = useMemo(() => {
  //   return slowFunction(count);
  // }, [count]);

  // const addPromise = new Promise((resolve, reject) => {
  //   const total2 = slowFunction(count);
  //   if (count < 0) {
  //     reject("Number turns negative");
  //   } else {
  //     resolve(total2);
  //   }
  // });wrong

  const themeStyles = {
    backgroundColor: dark ? "black" : "white",
    color: dark ? "white" : "black",
  };

  // addPromise
  //   .then((data) => {
  //     console.log("load");
  //     console.log(data);
  //   })
  //   .catch((err) => {
  //     console.log(err);
  //   });

  return (
    <div>
      <h1>Total added number {count}</h1>
      <button onClick={() => onClickHandler()}>Add 1</button>
      <button onClick={() => setCount(count - 1)}>Subtract 1</button>
      <button onClick={() => setDark(!dark)}>Change Color</button>
      {/* <div style={themeStyles}>{addNumber}</div> */}
    </div>
  );
};

export default CountPage;

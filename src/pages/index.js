import MainPage from "./MainPage";
import ElementPage from "./ElementPage";
import ElementList from "./ElementList";
import HomePage from "./HomePage";
import PokeDexPage from "./PokeDexPage";
export { MainPage, ElementPage, ElementList, HomePage, PokeDexPage };

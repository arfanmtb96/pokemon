import { useParams } from "react-router-dom";
import React, { lazy, Suspense } from "react";
import useFetch from "../../components/Fetch/useFetch";
// import CardList from "../../components/cardsList/CardList";
const CardList = lazy(() => import("../../components/cardsList/CardList"));

const ElementList = () => {
  const { Elements } = useParams();
  const { searchData: types, error, isPending } = useFetch(
    `https://api.pokemontcg.io/v2/cards?q=supertype:pokemon%20types:${Elements}`
  );
  // console.log("test " + types);
  return (
    <div>
      <h2 className="bg-dark mt-2 text-white p-2 text-center">
        Type-{Elements}
      </h2>
      <Suspense
        fallback={<h1 className="text-center mt-5">Wait a sec.....</h1>}
      >
        {error && <div>{error}</div>}
        {/* {isPending && <div>Loading...</div>} */}

        {types && <CardList cards={types} />}
      </Suspense>
    </div>
  );
};

export default ElementList;

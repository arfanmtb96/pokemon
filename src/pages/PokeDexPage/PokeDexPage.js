import { useEffect, useState } from "react";
import PokemonList from "../../components/pokemonList/PokemonList";
import PageButton from "../../components/pageButton/PageButton";
import "./PokeDexPage.css";

const PokeDexPage = () => {
  const [pokemon, setPokemon] = useState([]);
  const [currentPageUrl, setCurrentPageUrl] = useState(
    "https://pokeapi.co/api/v2/pokemon"
  );
  const [nextPageURL, setNextPageUrl] = useState();
  const [prevPageURL, setPrevPageUrl] = useState();

  useEffect(() => {
    fetch(currentPageUrl)
      .then((res) => res.json())
      .then((data) => {
        const { results, previous, next } = data;
        setNextPageUrl(next);
        setPrevPageUrl(previous);
        setPokemon(results);
      });
  }, [currentPageUrl]);
  function gotoNextPage() {
    setCurrentPageUrl(nextPageURL);
  }

  function gotoPreviousPage() {
    setCurrentPageUrl(prevPageURL);
  }

  return (
    <div className="container-md p-3 my-3 bg-danger text-white">
      <div className="container-md p-3 my-2 bg-light border-2 rounded">
        <div className="container-md p-4 bg-dark rounded">
          <div className="container-md p-2 bg-warning rounded">
            <PokemonList pokemon={pokemon} />
          </div>
        </div>
      </div>
      <div className="right-container__buttons">
        <PageButton
          gotoNextPage={nextPageURL ? gotoNextPage : null}
          gotoPreviousPage={prevPageURL ? gotoPreviousPage : null}
        />
      </div>
    </div>
  );
};

export default PokeDexPage;

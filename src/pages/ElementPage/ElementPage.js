import { useState } from "react";
import { Link } from "react-router-dom";

import "./ElementPage.css";
import Colorless from "../../assets/Colorless.png";
import Darkness from "../../assets/Darkness.png";
import Dragon from "../../assets/Dragon.png";
import Fairy from "../../assets/Fairy.png";
import Fighting from "../../assets/Fighting.png";
import Fire from "../../assets/Fire.png";
import Grass from "../../assets/Grass.png";
import Lightning from "../../assets/Lightning.png";
import Metal from "../../assets/Metal.png";
import Psychic from "../../assets/Psychic.png";
import Water from "../../assets/Water.png";
// import ElementList from "./ElementList";

const ElementPage = () => {
  const [type, setType] = useState([
    { image: Colorless, name: "Colorless", id: 1 },
    { image: Darkness, name: "Darkness", id: 2 },
    { image: Dragon, name: "Dragon", id: 3 },
    { image: Fairy, name: "Fairy", id: 4 },
    { image: Fighting, name: "Fighting", id: 5 },
    { image: Fire, name: "Fire", id: 6 },
    { image: Grass, name: "Grass", id: 7 },
    { image: Lightning, name: "Lightning", id: 8 },
    { image: Metal, name: "Metal", id: 9 },
    { image: Psychic, name: "Psychic", id: 10 },
    { image: Water, name: "Water", id: 11 },
  ]);
  return (
    <div className="bg-dark">
      <div className="d-flex flex-row flex-wrap justify-content-center">
        {type.map((types) => (
          <div key={types.id} className="element-container m-4">
            <Link to={`/ElementPage/${types.name}`}>
              <img src={types.image} alt={types.name} className="image" />
              <div className="middle">
                <div className="text">{types.name}</div>
              </div>
            </Link>
          </div>
        ))}

        {/* <ElementList image={type} title="All Element Type" /> */}
      </div>
    </div>
  );
};

export default ElementPage;

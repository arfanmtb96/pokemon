import "./MainPage.css";
import { React, useState } from "react";
import useFetch from "../../components/Fetch/useFetch";
import CardList from "../../components/cardsList/CardList";
import "../../../node_modules/bootstrap/dist/css/bootstrap.min.css";

const MainPage = () => {
  const [searchInput, setSearchInput] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    // const test = { searchInput };
  };
  const { searchData: cards, loading, error } = useFetch(
    `https://api.pokemontcg.io/v2/cards?q=name:${searchInput}`
  );

  return (
    <div>
      <div className="navbar bg-dark">
        <div className="flex-grow-1"></div>
        <form onSubmit={handleSubmit} className="d-flex p-2">
          <input
            value={searchInput}
            onChange={(e) => setSearchInput(e.target.value)}
            placeholder="Search your Pokemon"
          />
          <button className="btn btn-secondary " type="submit">
            {" "}
            Search
          </button>
        </form>
      </div>
      {error && <div className="text-center">{error}</div>}
      <br></br>
      {loading && <div className="text-center">Loading...</div>}
      <div className="d-flex justify-content-center">
        {cards && <CardList cards={cards} />}
      </div>
    </div>
  );
};

export default MainPage;

import "./PokemonList.css";
import { Link } from "react-router-dom";

const PokemonList = ({ pokemon }) => {
  return (
    <div className="d-flex flex-wrap ">
      {pokemon.map((p, i) => (
        <div className="text-capitalize" key={p.name}>
          {/* <div onClick=""></div> */}
          <Link to={`/PokeDexPage/${p.name}`}>
            {i + 1}. {p.name}
          </Link>
        </div>
      ))}
    </div>
  );
};

export default PokemonList;

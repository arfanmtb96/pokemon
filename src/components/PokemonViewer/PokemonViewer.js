import "./PokemonViewer.css";
import { useParams, Link } from "react-router-dom";
import { useEffect, useState } from "react";
import abra from "../../assets/63.wav";

let audioObj = null;

const PokemonViewer = () => {
  const { Pokemon } = useParams();
  const [pokemonUrl, setPokemonUrl] = useState(
    `https://pokeapi.co/api/v2/pokemon/${Pokemon}`
  );
  const [pokemonImage, setPokemonImage] = useState([]);
  const [pokemonType, setPokemonType] = useState([]);
  const [pokemonWeight, setPokemonWeight] = useState([]);
  const [pokemonHeight, setPokemonHeight] = useState([]);
  const [pokemonFirstType, setPokemonFirstType] = useState([]);
  const [pokemonID, setPokemonID] = useState([]);
  const [pokemonName, setPokemonName] = useState([]);

  useEffect(() => {
    fetch(pokemonUrl)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        const { id, name, sprites, types, height, weight } = data;
        setPokemonID(id);
        setPokemonName(name);
        setPokemonImage(sprites);
        setPokemonType(types);
        setPokemonHeight(height);
        setPokemonWeight(weight);

        // console.log(data);
        const { slot, type } = types[0];
        setPokemonFirstType(type.name);
      });
  }, [pokemonUrl]);

  const audioHandler = (test) => {
    audioObj = new Audio(`https://pokemoncries.com/cries-old/${test}.mp3`);

    audioObj.play();
  };
  return (
    <div className="pokedex">
      <div className="left-container">
        <div className="left-container__top-section">
          <div className="top-section__blue"></div>
          <div className="top-section__small-buttons">
            <div className="top-section__red"></div>
            <div className="top-section__yellow"></div>
            <div className="top-section__green"></div>
          </div>
        </div>
        <div className="left-container__main-section-container">
          <div className="left-container__main-section">
            <div className="main-section__white">
              <div className="main-section__black">
                <div className={pokemonFirstType}>
                  <div className="main-screen">
                    <div className="screen__header ">
                      <span className="poke-name text-capitalize">
                        {pokemonName}
                      </span>
                      <span className="poke-id">#{pokemonID}</span>
                    </div>
                    <div className="screen__image">
                      <img
                        src={pokemonImage.front_default}
                        className="poke-front-image"
                        alt="front"
                      ></img>
                      <img
                        src={pokemonImage.back_default}
                        className="poke-back-image"
                        alt="back"
                      ></img>
                    </div>
                    <div className="screen__description">
                      <div className="stats__types">
                        {pokemonType.map(({ slot, type }) => (
                          <div
                            className="text-capitalize text-white poke-type-one"
                            key={slot}
                          >
                            {type.name}
                          </div>
                        ))}
                      </div>
                      <div className="screen__stats">
                        <p className="text-white">
                          Weight:{" "}
                          <span className="text-white">{pokemonWeight}</span>
                        </p>
                        <p className="text-white">
                          Height:{" "}
                          <span className="text-white">{pokemonHeight}</span>
                        </p>
                      </div>
                      <button
                        className="sound-btn"
                        onClick={() => audioHandler(pokemonID)}
                      >
                        Sound
                      </button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default PokemonViewer;

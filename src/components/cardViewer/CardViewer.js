import "./CardViewer.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useParams } from "react-router-dom";
import { useEffect, useState } from "react";

const CardViewer = () => {
  console.log("arrive");
  const { ID } = useParams();
  const [cardUrl, setCardUrl] = useState(
    `https://api.pokemontcg.io/v2/cards/${ID}`
  );
  const [pokemonImage, setPokemonImage] = useState([]);
  const [pokemonID, setPokemonID] = useState([]);
  const [pokemonName, setPokemonName] = useState([]);
  const [cardArtist, setCardArtist] = useState([]);

  useEffect(() => {
    fetch(cardUrl)
      .then((res) => res.json())
      .then((data) => {
        // console.log(data);
        const { id, name, artist } = data;
        setPokemonID(id);
        setPokemonName(name);
        setCardArtist(artist);
        console.log(pokemonName);
      });
  }, [cardUrl]);
  return (
    <div>
      test
      <div className="flex-container">
        <div>
          <p>Illustrate by {cardArtist}</p>
        </div>
      </div>
    </div>
  );
};

export default CardViewer;

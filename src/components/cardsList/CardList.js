import "./CardList.css";
import { LazyLoadImage } from "react-lazy-load-image-component";
import { useEffect, useState } from "react";
import Modal from "react-modal";

Modal.setAppElement("#root");

const CardList = ({ cards }) => {
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [selectedCardDetails, setSelectedCardDetails] = useState(null);

  const handleClick = (card) => {
    setModalIsOpen(true);
    setSelectedCardDetails(card);
  };

  return (
    <div className="flex-container">
      {cards.map((card) => (
        <div className="image" key={card.id}>
          <LazyLoadImage
            effect="blur"
            src={card.images.small}
            alt={card.name}
            placeholderSrc={
              "https://i.pinimg.com/originals/bc/4d/fa/bc4dfaa0dc931530793edf074a2cb411.png"
            }
            onClick={() => handleClick(card)}
          />
          {/* <button onClick={() => handleClick(card)}>Show more</button> */}

          {/* <p>{card.id}</p>
          <p>Illustrate by {card.artist}</p> */}
        </div>
      ))}

      {selectedCardDetails && (
        <Modal
          isOpen={modalIsOpen}
          onRequestClose={() => setModalIsOpen(false)}
        >
          <div>
            <h2>{selectedCardDetails.name}</h2>
            <LazyLoadImage
              className="big_image"
              effect="blur"
              src={selectedCardDetails.images.large}
              alt={selectedCardDetails.name}
              placeholderSrc={
                "https://i.pinimg.com/originals/bc/4d/fa/bc4dfaa0dc931530793edf074a2cb411.png"
              }
            />
            <p>Illustrate by {selectedCardDetails.artist}</p>
            {/* <code>
              {Object.entries(selectedCardDetails).map(([key, cardDetails]) => (
                <small key={key}>
                  {key}: {JSON.stringify(cardDetails)}
                  <br />
                </small>
              ))}
            </code> */}
            <button onClick={() => setModalIsOpen(false)}>Close</button>
          </div>
        </Modal>
      )}
    </div>
  );
};

export default CardList;

import { useEffect, useState } from "react";

const useFetch = (url) => {
  // const BASE_API_SEARCH_NAME = "https://api.pokemontcg.io/v2/cards?q=name:";
  const [loading, setLoading] = useState(true);
  const [searchData, setSearchData] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    const abortCont = new AbortController();
    // setTimeout(() => {
    fetch(url)
      .then((res) => {
        console.log(res);
        if (!res.ok) {
          throw Error("Could not fetch the data for that resource");
        }
        return res.json();
      })
      .then(({ data }) => {
        setSearchData(data);
        setLoading(false);
        setError(null);
      })
      .catch((err) => {
        if (err.name === "AbortError") {
          console.log("fetch aborted");
        } else {
          setLoading(false);
          setError(err.message);
        }
      });
    // }, 1000);

    return () => console.log("cleanup");
  }, [url]);
  return { searchData, loading, error };
};

export default useFetch;

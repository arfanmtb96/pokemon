import "./Navbar.css";
import { Link } from "react-router-dom";

const Navbar = () => {
  return (
    <nav className="navbar bg-danger">
      <Link to="/" className="text-white font-weight-bold">
        Pokemon
      </Link>
      <div className="links">
        <Link
          className="text-white hover-overlay"
          data-mdb-ripple-color="dark"
          to="/SearchPage"
        >
          Search Card
        </Link>
        <Link className="text-white" to="/ElementPage">
          Elements
        </Link>
        {/* <Link className="text-white" to="/CountPage">
          Count
        </Link> */}
        <Link className="text-white" to="/PokeDexPage">
          PokeDex
        </Link>
      </div>
    </nav>
  );
};

export default Navbar;

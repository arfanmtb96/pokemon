import React from "react";

const PageButton = ({ gotoNextPage, gotoPreviousPage }) => {
  return (
    <div>
      {gotoPreviousPage && (
        <button
          type="button"
          className="btn btn-secondary"
          onClick={gotoPreviousPage}
        >
          Previous
        </button>
      )}
      {gotoNextPage && (
        <button
          type="button"
          className="btn btn-secondary"
          onClick={gotoNextPage}
        >
          Next
        </button>
      )}
    </div>
  );
};

export default PageButton;
